
# Logical architecture
```plantuml
cloud "External" {
  cloud "Google" {
    interface "G Oauth2" as GOauth2
    interface "GMail REST" as GREST
  }
  cloud "Microsoft" {
    interface "M Oauth2" as MOath2
    interface "M GraphQL" as MGraph
  }
  cloud "Ext SMTP" {
    interface SMTP
  }
}



cloud "OneMail" {
  folder "Private" as OneMailPriv {
    database "Storage" {
      [Messages]
      [Users]
    }

    [Auth Helper]
    [Auth Helper] <-> [Users]
    [Auth Helper] <-> MOath2
    [Auth Helper] <-> GOauth2

    [SMTP Connector]
    [SMTP Connector] <-> SMTP

    [AWS SES Connector]
    
    
    [AWS SES Connector] -> [Messages]
    
  }
  folder "Public" as OneMailPub {
    [GraphQL]
  }
  OneMailPriv - OneMailPub
}


package "Common" {
  interface "Connector"

  [GMail Connector] .. Connector
  [Outlook Connector] .. Connector
  [Onemail Connector] .. Connector
  

  [GMail Connector] <-> GREST
  [Outlook Connector] <-> MGraph
}

node "Client" {
  folder "Components" {
    folder "Basic" {
      ' Basic UI. List messages, read messages, write
      [Message list]
      [Message panel]
      [Write panel]
    }
    folder "Auth" {
      ' User things. Add users etc
      [User select]
      [User register]
      [Add SMTP]
      [Register Google]
      [Register Microsoft]
    }    
  }
  folder "Logic" {
    [Search]
    database "Local Storage"
  }
  
  Common - Client
}
```
Here's the entire architecture with external data providers also shown.

As you can see the APIs available to be used with HTTPS protocol are going to be usable with client-side only. These can also be logged in to the serverside to enable sharing the login status between multiple devices.

# Container arch
```plantuml
node Connector
folder Core {
  node GraphQL
  database DB
  GraphQL - DB
}
GraphQL <-* Connector
```
The GraphQL provides REST API interface to register data sources (Connectors). 

When requesting the GraphQL interface it finds all the connectors that can be utilized with given credentials and allows usage of those.