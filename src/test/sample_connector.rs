use crate::connector::connector::Connector;
use crate::mail::types::Mail;
use std::collections::HashMap;

struct SampleConnector {}

impl Connector for SampleConnector {
  fn new() -> SampleConnector {
    return SampleConnector {};
  }
  fn send_mail(&self, _mail: Mail) -> bool {
    return true;
  }
  fn get_mail(&self, _addr: String) -> Vec<Mail> {
    return vec![Mail {
      from: "sender@example.com".to_string(),
      to: "receiver@example.com".to_string(),
      headers: HashMap::new(),
      content: "Moi".to_string(),
    }];
  }
}

#[test]
fn test_sample_connector() {
  let connector = SampleConnector::new();
  let mail = connector.get_mail("asdf@example.com".to_string());
  assert_eq!(mail[0].from, "sender@example.com");
  assert_eq!(mail[0].to, "receiver@example.com");
  assert_eq!(mail[0].headers.len(), 0);
  assert_eq!(mail[0].content, "Moi");
}
