# List of Components in OneMail architecture

## Common
- Email provider interface (Connector)

## Core
- Database (MongoDB / NoSQL)
  - Logins
  - Email storage for stateless connector services
  - Bulk storage provider interface
    - AWS S3 / other blob storage support
- SMTP proxy
  - Allow retrieving emails via graphql endpoint from SMTP services
- GraphQL server
  - Exposes nice interface to clients

## Connectors
- GMail
  - Uses their API
    - [https://developers.google.com/gmail/api]
  - Uses google Oauth2
  - Client side only support
- Outlook
  - Uses Microsoft graphql
    - [https://developer.microsoft.com/en-us/graph/graph-explorer]
    - Uses microsoft Oauth2
  - Client side only support
- SMTP
  - Allows for interfacing with SMTP servers via graphql proxy
  - Authorization with OneMail session handling (User db)
    or with user provided credentials in query
- AWS SES
  - Stateless service to send/receive email
  - Lambda handler for incoming email
  - Outgoing sent with AWS SDKs with connector microservice
  - Support for storing emails in OneMail storage

