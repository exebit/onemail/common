use std::collections::HashMap;

pub struct Mail {
  // Use heap allocated strings as size is not fixed
  pub from: String,
  pub to: String,
  pub headers: HashMap<String, String>,
  pub content: String,
}
