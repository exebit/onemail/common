use crate::mail::types::Mail;

pub trait Connector {
  fn new() -> Self;
  fn send_mail(&self, mail: Mail) -> bool;
  fn get_mail(&self, addr: String) -> Vec<Mail>;
}
