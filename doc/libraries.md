# Libraries used in OneMail
The entire codebase should use Rust whenever possible.

## Database
For database connectivity the models will be declared in struct style.

This allows us to use one of the following database libraries:
- [Dynomite](https://github.com/softprops/dynomite) (AWS DynamoDB)
- [MongoDB](https://github.com/mongodb/mongo-rust-driver)
  - Object serilization with Serdo. See: [bson-rust](https://github.com/mongodb/bson-rust)

## GraphQL
Choose some of the options for 

### Client side
- [graphql-client](https://github.com/graphql-rust/graphql-client)
- [stdweb](https://github.com/koute/stdweb) and just use fetch
- [reqwest](https://github.com/seanmonstar/reqwest)

### Server side
- [Juniper](https://github.com/graphql-rust/juniper) for the login
- [Actix](https://github.com/actix/examples/tree/master/juniper) or [Hyper](https://github.com/hyperium/hyper)

## External APIs
Outlook: ?  
Gmail: ?  
AWS SES: [rusoto](https://github.com/rusoto/rusoto)  