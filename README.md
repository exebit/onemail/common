# OneMail - Shared components

Shared components of OneMail. Architecture diagrams, data models, etc

## Datamodels
- User
  - Logins
- Message

## Traits / REST specs
- Connector
- Authentication

## GraphQL
- SDL spec

# Workspace helpers
There are recommended vscode extensions listed in .vscode folder.  
See also the tasks defined in tasks.json to run PlantUML server and test/build the package.